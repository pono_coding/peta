<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->module('layout');
		$this->load->model('ref/m_ref');
	}

	function index()
	{
		$company = $this->m_ref->getCompany();
		$data 	 = array('company' => $company[0]['name'], 'title' => 'Complaint');

		$this->layout->header($data);
		$this->layout->menu();
		$this->load->view('v_about');
		$this->layout->footer();
	}
}