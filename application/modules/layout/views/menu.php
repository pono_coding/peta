
    <nav class="navbar navbar-expand-sm navbar-teal fixed-top">
        <a class="navbar-brand" href="<?php echo base_url('home'); ?>">
          <img src="<?php echo base_url(); ?>assets/img/logo_pemkot.png" width="120" height="" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('home'); ?>">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('about'); ?>">Tentang</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Dokumentasi</a>
                </li>
            </ul>
        </div>

        <div style="text-align: right;">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-outline-dark my-2 my-sm-0 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Akun
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item">Profil</a>
                    <a href="#" class="dropdown-item" type="button">Keluar</a>
                </div>
            </div>
        </div>
    </nav>