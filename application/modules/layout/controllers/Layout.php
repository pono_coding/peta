<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Layout extends MX_Controller {

	function header($data = NULL)
	{
		$this->load->view('header', $data);
	}

	function menu()
	{
		$this->load->view('menu');
	}

	function footer()
	{
		$this->load->view('footer');
	}

}