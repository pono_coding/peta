		
		<style>
	        div.preview {max-width:250px;height:150px;border:2px solid #90A4AE; border-radius: 6px;}
	        img.preview {width:100%;height:100%;}
	    </style>

		<div class="container" style="margin-top:6%;">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
			                <strong class="card-title">Layanan Pelaporan</strong>
			            </div>
					</div>
					<form class="form" action="<?php echo base_url(); ?>" method="post" enctype="multipart/form-data">
					<div class="card">
						<div class="card-body">
							<div class="container" style="margin-top:2px">
								<h5 class="card-title">Data Pemohon</h5><hr>
							    <div class="row">
							    	<div class="col-sm-6">
								    	<div class="form-group">
											<label for="name">Akun JSS</label>
										  	<input type="text" class="form-control" id="akun_jss" name="akun_jss">
										</div>
								    </div>
								    <div class="col-sm-6">
								    	<div class="form-group">
										  	<label for="name">NIK Pemohon</label>
										  	<input type="text" class="form-control" id="nik" name="nik">
										</div>
								    </div>
							    </div>
							    <div class="row">
							    	<div class="col-sm-6">
								    	<div class="form-group">
											<label for="name">Nama Lengkap Pemohon</label>
										  	<input type="text" class="form-control" id="name" name="name">
										</div>
								    </div>
								    <div class="col-sm-6">
								    	<div class="form-group">
										  	<label for="name">No. HP</label>
										  	<input type="text" class="form-control" id="phone" name="phone">
										</div>
								    </div>
							    </div>
							</div>
						</div>
						<div class="card-body">
							<div class="container" style="margin-top:2px">
								<h5 class="card-title">Pelayanan dan Lokasi</h5><hr>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Waktu</label>
									<div class="col-sm-9">
										<input type="text" class="form-control datepicker" name="time" id="time">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Keterangan</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="desc" id="desc">
									</div>
								</div>
								<input type="hidden" class="form-control" name="latitude" id="latitude" readonly>
								<input type="hidden" class="form-control" name="longitude" id="longitude" readonly>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Lokasi</label>
									<div class="col-sm-9">
										<div id="map" style="height:450px;width:750px;"></div>
										<input id="pac-input" class="form-control" style="margin-top:12px;width:60%;" type="text" placeholder="Search Place" size="10"><br>
										<input type="text" class="form-control" name="lokasi" id="lokasi" readonly>
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Lokasi</label>
									<div class="col-sm-9">
										<div id="map2" style="height:450px;width:750px;"></div>
										<input id="pac-input2" class="form-control" style="margin-top:12px;width:60%;" type="text" placeholder="Search Place" size="10"><br>
										<input type="text" class="form-control" name="lokasi" id="lokasi" readonly>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Pilihan Berkas Unggahan</label>
									<div class="col-sm-9">
										<div class="preview">
											<img class="preview" id="prevImagePhoto" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" />
										</div><br>
										<input type="file" class="form-control-file" name="photo" onchange="readURLPhoto(this)">
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<div class="container" style="margin-top:2px">
								<div class="row">
							    	<div class="col-sm-12 text-right">
								    	<div class="form-group">
										    <button type="submit" class="btn btn-primary" id="btnAdd">Kirim Pengajuan Permohonan</button>
								    	</div>
								    </div>
								</div>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>

		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVHpUSMf6VsoMPEiNIoTOlLIR48r7qSs8&libraries=places&callback=initAutocomplete&hl=id" async defer></script>
	    <script type="text/javascript">
	    	$(function(){
				$(".datepicker").datepicker({
					format: 'dd-mm-yyyy',
					autoclose: true,
					todayHighlight: true,
				});
	        });
	        
	        function readURLPhoto(input) {
	            if (input.files && input.files[0]) {   
	                var reader = new FileReader();
	                reader.onload = function (e)
	                {document.getElementById('prevImagePhoto').src=e.target.result;}
	                reader.readAsDataURL(input.files[0]);
	            }
	        }

		    var marker;
		    var map;
		    var map2;
		    var myLatLng;
		    var mapDiv;
		    var myPos;
		    var latval;
		    var lngval;

		    function initAutocomplete() {
		    	var geocoder = new google.maps.Geocoder;
			    var infowindow = new google.maps.InfoWindow;

		        // myLatLng  = {lat: -2.1527342, lng: 112.2539421};
		        mapDiv    = document.getElementById('map');

		        latval = document.getElementById('latitude').value;
		        lngval = document.getElementById('longitude').value;
				
		        if (latval == "" && lngval == "") {
		            myLatLng    = {lat: -7.782889857583351, lng: 110.36707386374474};
		            myPos       = null;
		        } else {
		            myLatLng    = {lat: Number(latval), lng: Number(lngval)};
		            myPos       = {lat: Number(latval), lng: Number(lngval)};
		        };

		        map = new google.maps.Map(mapDiv, {
		            center: myLatLng,
		            zoom: 10,
		            mapTypeId: google.maps.MapTypeId.ROADMAP
		        });
				
				var input = document.getElementById('pac-input');

		        var searchBox = new google.maps.places.SearchBox(input);
		        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		        input.style.display = 'block';

		        //event if place changed
		        searchBox.addListener('places_changed', function() {
		          var places = searchBox.getPlaces();

		          if (places.length == 0) {
		            return;
		          }

		          marker.setMap(null);
		          marker.setPosition(null);
		          // For each place, get the icon, name and location.
		          var bounds = new google.maps.LatLngBounds();
		          places.forEach(function(place) {

		            marker.setMap(map);
		            marker.setTitle(place.name);

		            if (place.geometry.viewport) {
		              // Only geocodes have viewport.
		              bounds.union(place.geometry.viewport);
		            } else {
		              bounds.extend(place.geometry.location);
		            }
		          });
		          map.fitBounds(bounds);
		          //map.setZoom();
		        });

		        // default marker
		        marker = new google.maps.Marker({
		            position	: myPos,
		            map			: map,
		            draggable	: true,
		            animation	: google.maps.Animation.DROP,
		            title		: 'Set Position'
		        });

		        // add map even listener click
		        google.maps.event.addListener(map, 'click', function(event) {
		            setMarker(event.latLng);
		            var latitude    = event.latLng.lat();
		            var longitude   = event.latLng.lng();
		            setInput(latitude, longitude);
		            
		            geocodeLatLng(geocoder, map, infowindow, latitude, longitude, "#lokasi");
		        });

		        // add map even listener dragend marker
		        google.maps.event.addListener(marker, 'dragend', function(event) {
		            setMarker(event.latLng);
		            var latitude    = event.latLng.lat();
		            var longitude   = event.latLng.lng();
		            setInput(latitude, longitude);

		            geocodeLatLng(geocoder, map, infowindow, latitude, longitude, "#lokasi");
		        });
		    }

		    function setMarker(location) {
		        marker.setPosition(location);
		    }

		    function setInput(lat, lng) {
		        var inlat 	= document.getElementById('latitude');
		        var inlng 	= document.getElementById('longitude');
				
		        inlat.value = lat;
		        inlng.value = lng;
		    }

		    function geocodeLatLng(geocoder, map, infowindow, lat, lng, alamat) {
			    var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
			    geocoder.geocode({'location': latlng}, function(results, status) {
			        if (status === 'OK') {
			            if (results[0]) {
			                $(alamat).val(results[0].formatted_address);
			            } else {
			                window.alert('No results found');
			            }
			        } else {
			            window.alert('Geocoder failed due to: ' + status);
			        }
			    });
			}
		</script>
		