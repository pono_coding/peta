<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->module('layout');
		$this->load->model('ref/m_ref');
		$this->load->model('m_complaint');
	}

	function index()
	{
		$company = $this->m_ref->getCompany();
		$data 	 = array('company' => $company[0]['name'], 'title' => 'Complaint');

		$this->layout->header($data);
		$this->layout->menu();
		$this->load->view('v_complaint');
		$this->layout->footer();
	}

	public function save()
	{
		$this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('location', 'location', 'required');
        $this->form_validation->set_rules('message', 'message', 'required');

        if ($this->form_validation->run() == FALSE) {
        	$arr = array(
				'result'	=> 'warning',
				'content'	=> 'Field is required',
				'message'	=> $this->form_validation->error_array()
			);
        } else {
        	$photo = '';
        	$type  = '';
        	$size  = '';
        	$path  = '';

        	$this->load->library('upload');

            $config['upload_path']    = 'assets/upload_img/';
            $config['allowed_types']  = 'jpg|png|jpeg';
            $config['encrypt_name']   = TRUE;
            $config['max_size']       = '500000';
            $config['max_width']      = '2048';
            $config['max_height']     = '2048';

            $this->upload->initialize($config);

            if (!empty($_FILES['photo']['name'])) {
                if ($this->upload->do_upload('photo')) {
                    $image = $this->upload->data();
                    $photo = $this->resize($image['file_name']);
                    $type  = $image['image_type'];
                    $size  = $image['file_size'];
                    $path  = $image['file_path'];
                }
            }

        	$data 	= array(
						'complaint_reporter' 	=> $this->input->post('name'),
						'complaint_location' 	=> $this->input->post('location'),
						'complaint_report' 		=> $this->input->post('message'),
						'complaint_date_report' => date('Y-m-d'),
						'complaint_status' 		=> 1,
						'complaint_photo_file' 	=> $photo,
						'complaint_photo_path' 	=> $path,
						'complaint_photo_type' 	=> $type,
						'complaint_photo_size' 	=> $size
					  );
        	$data   = $this->security->xss_clean($data);
        	$result = $this->m_complaint->insert($data);

        	if ($result) {
        		$arr = array(
					'result'	=> 'success',
					'content'	=> $result,
					'message'	=> 'Data has been saved successfully'
				);
        	} else {
    			$arr = array(
					'result'	=> 'failed',
					'content'	=> $result,
					'message'	=> 'Data not saved successfully'
				);
        	}
        }

        echo json_encode($arr);
	}

	public function resize($file)
	{
		$config = array();

        $config['image_library'] 	= 'gd2';
        $config['source_image']  	= 'assets/upload_img/'.$file;
        $config['create_thumb'] 	= FALSE;
        $config['maintain_ratio'] 	= FALSE;
        $config['quality']			= '60%';
        $config['width']			= 600;
        $config['height']			= 400;

        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        unset($this->image_lib);
        $photo = $file;

		return $photo;
	}
	
}