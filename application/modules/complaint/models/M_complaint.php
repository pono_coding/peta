<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_complaint extends CI_Model {
 
    function __construct ()
    {
    	parent::__construct();
        $this->table_name = 'complaint';
    }

    function insert($data)
    {
        return $this->db->insert($this->table_name, $data);
    }

}