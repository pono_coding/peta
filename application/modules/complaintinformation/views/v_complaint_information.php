
	<!--================ Start Element Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
			<div class="container">
				<div class="banner_content text-left">
					<div class="page_link">
						<a href="<?php echo base_url('home'); ?>">Home</a>
						<a href="<?php echo base_url('complaintinformation'); ?>">Keluhan</a>
					</div>
					<h2>Daftar Laporan Keluhan</h2>
				</div>
			</div>
		</div>
	</section>
	<!--================ End Features Banner Area =================-->

	<!--================ Page Content ================-->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<div class="progress-table-wrap">
					<div class="progress-table">
						<div class="row">
					      <div class="col-lg-12">
					        <h2>Daftar Laporan Keluhan</h2>
					      </div>
					      <div class="col-lg-12">
					      	<div class="card-body">
                                <form class="form-horizontal form-label-left" method="POST" action="<?php echo base_url('complaintinformation'); ?>">
                                    <div class="row form-group">
                                        <div class="col col-md-2"><label for="nama-produk" class=" form-control-label">Pesan</label></div>
                                        <div class="col-12 col-md-10">
                                            <input type="text" class="input-sm form-control-sm form-control" name="message" value="<?php echo $message; ?>" placeholder="Pesan">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-2"><label for="" class=" form-control-label">&nbsp;</label></div>
                                        <div class="col-12 col-md-10">
                                            <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
					      </div>
					      <div class="col-lg-12">
					      	<div class="table-responsive-sm">
							  <table class="table">
							      <thead>
								    <tr>
								      <th scope="col">#</th>
								      <th scope="col">Foto</th>
								      <th scope="col">Lokasi</th>
								      <th scope="col">Pesan</th>
								      <th scope="col">Pelapor</th>
								      <th scope="col">Tanggal</th>
								      <th scope="col">Status</th>
								      <th scope="col">Tanggapan</th>
								      <th scope="col">Aksi</th>
								    </tr>
								  </thead>
								  <tbody>
								    <?php if(!empty($data)) { ?>
                                    <?php $no = $this->uri->segment('3') + 1; ?>
                                    <?php foreach ($data as $row) :?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><img class="img-rounded img-thumbnail" src="<?php echo base_url(); ?>assets/upload_img/<?php echo $row['photo_file']; ?>" alt="Foto"></td>
                                            <td><?php echo $row['location']; ?></td>
                                            <td><?php echo $row['message']; ?></td>
                                            <td><?php echo $row['name']; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($row['date'])); ?></td>
                                            <td><?php echo $row['status_name']; ?></td>
                                            <td><?php echo $row['response']; ?></td>
                                            <td>
	                                            <a href="<?php echo base_url('complaintinformation/detail/').$row['id']; ?>" class="btn btn-info btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
	                                        </td>
                                        </tr>
                                    <?php $no++; ?>
                                    <?php endforeach; ?>
                                  <?php } else{ ?>
                                    <tr>
                                      <td colspan="6" style="text-align: center;">-- Data Not Found --</td>
                                    </tr>
                                  <?php } ?>
								  </tbody>
							  </table>
							</div>
					      </div>
					      <div class="col-lg-12">
					      	<?php echo $pagination; ?>
					      </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--================ End Page Content ================-->