<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ComplaintInformation extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->module('layout');
		$this->load->model('ref/m_ref');
		$this->load->model('m_info_complaint');
	}

	function index()
	{
		$url 						= 'complaintinformation/index';
		$total 						= $this->m_info_complaint->totalData();
		$per_page 					= 3;
		$content['pagination'] 		= $this->paging->createPagination($url, $per_page, $total);
		$content['page']            = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$content['data']         	= $this->m_info_complaint->getData($per_page, $content['page'], $this->input->post('message'));

		$company 					= $this->m_ref->getCompany();
		$data 	 					= array('company' => $company[0]['name'], 'title' => 'Complaint Information');
		$content['message']         = $this->input->post('message');

		$this->layout->header($data);
		$this->layout->menu();
		$this->load->view('v_complaint_information',  $content);
		$this->layout->footer();
	}

	function detail()
	{
		$company = $this->m_ref->getCompany();
		$content = $this->m_info_complaint->getDataDetail($this->uri->segment(3));
		$data 	 = array('company' => $company[0]['name'], 'title' => 'Detail Complaint');
		
		$this->layout->header($data);
		$this->layout->menu();
		$this->load->view('v_complaint_information_detail',  $content);
		$this->layout->footer();
	}

}